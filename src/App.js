
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import image from "./assets/images/48.jpg";



function App() {
  return (
    <div >
      <div style={{width:"800px",margin: "0 auto",textAlign:"center", border:"1px solid #ddd", marginTop:"100px", paddingBottom:"50px",backgroundColor:"bisque"}}>
        
        <div style={{marginTop:"-50px"}}>
          <img  style={{width:"100px",borderRadius:"50%"}} src={image} alt='image user'></img>
        </div>

        <div >
          <p>This is one of the best developer blogs on the planet! I read it daily to improve my skills </p>
        </div>

        <div style={{fontSize:"12px",color:"brown"}}>
          <b style={{color:"blue"}}> Tammy Srevens </b> . Front End Developer
        </div>

      </div>
    </div>
  );
}

export default App;
